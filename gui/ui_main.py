
from PyQt5.QtWidgets import QLabel, QPushButton, QVBoxLayout, QWidget, QHBoxLayout

from utils.delLayout import delLayout
from widget.DrawWidget import DrawWidget


class TopBarParams:
    def __init__(self, newListener, aboutListener, resetListener, settingListener):
        self.newListener = newListener
        self.aboutListener = aboutListener
        self.resetListener = resetListener
        self.settingListener = settingListener


class FuncBarParams:
    def __init__(self, fcfsListener, sstfListener, scanListener, cscanListener):
        self.fcfsListener = fcfsListener
        self.sstfListener = sstfListener
        self.scanListener = scanListener
        self.cscanListener = cscanListener


class UI_Main:
    def __init__(self):
        self.page_list = None  # 当前页面列表，写法不规范
        self.draw_label = None  # 左侧窗口
        self.out_put = None  # 右侧窗口

    def refreshCurrent(self, current_pages):
        if len(current_pages) == 0:
            self.draw_label.reset()
            return
        self.draw_label.set_queue(current_pages)
        self.draw_label.set_head(current_pages[0])
        self.draw_label.update()
        pass

    def refreshOutput(self, out_str):
        delLayout(self.out_put)
        out_put = QLabel(out_str)
        self.out_put.addWidget(out_put)

    def refreshPages(self, list_input):
        delLayout(self.page_list)
        for item in list_input:
            obj_vg = QWidget()
            obj_vg.setStyleSheet("""
                background-color: #bcaaa4;
                """)

            obj = QPushButton("{}".format(item))
            obj_container = QVBoxLayout()
            obj_container.addWidget(obj)
            obj_vg.setLayout(obj_container)
            self.page_list.addWidget(obj_vg)
        self.page_list.addStretch()

    def createTopBar(self, top_bar_Params):
        container_widget = QWidget()
        container_widget.setStyleSheet("background-color: #5D4037;")

        top_bar = QHBoxLayout()

        newButton = QPushButton("随机生成")
        newButton.setStyleSheet("background-color: #D7CCC8;")
        settingButton = QPushButton("设置")
        settingButton.setStyleSheet("background-color: #D7CCC8;")
        aboutButton = QPushButton("关于")
        aboutButton.setStyleSheet("background-color: #D7CCC8;")
        resetButton = QPushButton("重置")
        resetButton.setStyleSheet("background-color: #D7CCC8;")
        newButton.clicked.connect(lambda: top_bar_Params.newListener(self))
        settingButton.clicked.connect(lambda: top_bar_Params.settingListener(self))
        aboutButton.clicked.connect(top_bar_Params.aboutListener)
        resetButton.clicked.connect(lambda: top_bar_Params.resetListener(self))
        top_bar.addWidget(newButton)
        top_bar.addWidget(settingButton)
        top_bar.addWidget(aboutButton)
        top_bar.addWidget(resetButton)
        top_bar.addSpacing(40)
        top_bar.addStretch()

        container_widget.setLayout(top_bar)
        return container_widget

    def createFuncBar(self, func_bar_Params):
        func_vg = QWidget()
        func_vg.setStyleSheet("""
                            background-color: #efebe9;
                            border-top-left-radius:10px;
                            border-top-right-radius:10px;
                            border-bottom-left-radius:10px;
                            border-bottom-right-radius:10px;
                            padding:2px 4px;
                            """)
        func_list = QHBoxLayout()
        fcfs_btn = QPushButton("FCFS")
        fcfs_btn.setStyleSheet("background-color: #d7ccc8;")
        fcfs_btn.clicked.connect(lambda: func_bar_Params.fcfsListener(self))
        sstf_btn = QPushButton("SSTF")
        sstf_btn.setStyleSheet("background-color: #d7ccc8;")
        sstf_btn.clicked.connect(lambda: func_bar_Params.sstfListener(self))
        scan_btn = QPushButton("SCAN")
        scan_btn.setStyleSheet("background-color: #d7ccc8;")
        scan_btn.clicked.connect(lambda: func_bar_Params.scanListener(self))
        cscan_btn = QPushButton("CSCAN")
        cscan_btn.setStyleSheet("background-color: #d7ccc8;")
        cscan_btn.clicked.connect(lambda: func_bar_Params.cscanListener(self))
        func_list.addWidget(fcfs_btn)
        func_list.addWidget(sstf_btn)
        func_list.addWidget(scan_btn)
        func_list.addWidget(cscan_btn)

        func_vg.setLayout(func_list)
        return func_vg

    def createChart(self):
        chart_vg = QHBoxLayout()

        wait_cg_container = QWidget()
        run_cg_container = QWidget()

        wait_cg_container.setStyleSheet("""
        background-color: #795548;
        border:4px solid #757de8; 
        border-top-left-radius:10px;
        border-top-right-radius:10px;
        border-bottom-left-radius:10px;
        border-bottom-right-radius:10px;
        padding:2px 4px;
        """)
        run_cg_container.setStyleSheet("""
        background-color: #795548;
        border:4px solid #757de8; 
        border-top-left-radius:10px;
        border-top-right-radius:10px;
        border-bottom-left-radius:10px;
        border-bottom-right-radius:10px;
        padding:2px 4px;
        """)

        wait_cg_container.setFixedHeight(300)
        run_cg_container.setFixedHeight(300)

        wait_cg = QVBoxLayout()
        run_cg = QVBoxLayout()

        chart_vg.addLayout(wait_cg)
        chart_vg.addLayout(run_cg)

        text_queue_wait = QLabel("调度图示")

        wait_cg.addWidget(text_queue_wait)
        wait_cg.addWidget(wait_cg_container)

        wait_list = QVBoxLayout()
        wait_cg_container.setLayout(wait_list)
        draw_label = DrawWidget(y_dip=20, x_scale=1.25)
        draw_label.setFixedHeight(300)
        wait_list.addWidget(draw_label)

        self.draw_label = draw_label

        text_queue_run = QLabel("输出")

        run_cg.addWidget(text_queue_run)
        run_cg.addWidget(run_cg_container)

        run_list = QVBoxLayout()
        run_cg_container.setLayout(run_list)
        self.out_put = run_list

        return chart_vg

    def createPageList(self):
        page_list_vg = QWidget()
        page_list_vg.setStyleSheet("""
                                   background-color: #efebe9;
                                   border-top-left-radius:10px;
                                   border-top-right-radius:10px;
                                   border-bottom-left-radius:10px;
                                   border-bottom-right-radius:10px;
                                   padding:2px 4px;
                                   """)
        self.page_list = QHBoxLayout()
        page_list_vg.setLayout(self.page_list)
        return page_list_vg

    def CreateMainWindow(self, screen, top_bar_params, func_bar_params):
        win = QWidget()
        win.setWindowTitle("磁盘调度")
        width = 800
        height = 600
        win.setGeometry((screen.width - width) / 2, (screen.height - height) / 2, width, height)

        outline_vg = QVBoxLayout()

        top_bar = self.createTopBar(top_bar_params)
        func_bar = self.createFuncBar(func_bar_params)
        chart = self.createChart()
        page_list = self.createPageList()

        outline_vg.addWidget(top_bar)
        outline_vg.addStretch(1)

        outline_vg.addWidget(QLabel("请求队列: "))
        outline_vg.addWidget(page_list)

        outline_vg.addWidget(QLabel("调度方法: "))
        outline_vg.addWidget(func_bar)

        outline_vg.addLayout(chart)
        outline_vg.addStretch(30)

        win.setLayout(outline_vg)

        return win
