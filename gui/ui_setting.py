
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QLineEdit, QPushButton

from PyQt5.QtGui import QIntValidator
import var


class SettingParams:
    def __init__(self, successListener):
        self.successListener = successListener


def CreateSettingWindow(screen, new_Params, ui_main):
    win = QDialog()
    win.setWindowFlags(Qt.WindowCloseButtonHint)
    win.setWindowTitle('设置')
    width = 400
    height = 300
    win.setGeometry((screen.width - width) / 2, (screen.height - height) / 2, width, height)

    outline_vg = QVBoxLayout()

    text_subline = QLabel("--------------------")
    outline_vg.addWidget(text_subline)

    outline_vg.addWidget(QLabel("时间间隔: "))

    time_split = QLineEdit(f"{var.time_split}")
    time_split.setValidator(QIntValidator())
    outline_vg.addWidget(time_split)

    outline_vg.addWidget(QLabel("磁头即将扫描的磁道号: "))

    next_id = QLineEdit(f"{var.next_id}")
    next_id.setValidator(QIntValidator())
    outline_vg.addWidget(next_id)

    outline_vg.addWidget(QLabel("最大磁道号: "))

    max_id = QLineEdit(f"{var.max_id}")
    max_id.setValidator(QIntValidator())
    outline_vg.addWidget(max_id)

    outline_vg.addWidget(QLabel("随机生成数量: "))

    rand_num = QLineEdit(f"{var.rand_num}")
    rand_num.setValidator(QIntValidator())
    outline_vg.addWidget(rand_num)

    outline_vg.addWidget(QLabel("磁道号减小的方向: "))
    outline_vg.addWidget(QLabel("0表示向磁道号减小的方向"))

    direct = QLineEdit(f"{var.direct}")
    direct.setValidator(QIntValidator())
    outline_vg.addWidget(direct)

    ok_button = QPushButton("确定")
    ok_button.clicked.connect(
        lambda: new_Params.successListener(win, time_split.text(), next_id.text(), max_id.text(), rand_num.text(),
                                           direct.text(), ui_main))
    outline_vg.addWidget(ok_button)

    outline_vg.addStretch()
    win.setLayout(outline_vg)
    return win
