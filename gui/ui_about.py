from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel

from var import author_text, version_text


def CreateAboutWindow(screen):
    win = QDialog()
    win.setWindowFlags(Qt.WindowCloseButtonHint)
    win.setWindowTitle('关于')
    width = 600
    height = 200
    win.setGeometry((screen.width - width) / 2, (screen.height - height) / 2, width, height)

    outline_vg = QVBoxLayout()

    text_first = QLabel(author_text)
    text_second = QLabel(version_text)

    outline_vg.addWidget(text_first)
    outline_vg.addWidget(text_second)

    win.setLayout(outline_vg)
    return win
