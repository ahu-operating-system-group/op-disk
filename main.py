import sys
from PyQt5.QtWidgets import QApplication

from control.func_control import cscan_click, scan_click, fcfs_click, sstf_click
from control.main_control import new_click, about_click, reset_click, setting_click
from gui.ui_main import TopBarParams, UI_Main, FuncBarParams
from var import screen

if __name__ == '__main__':
    mainApp = QApplication(sys.argv)

    UI = UI_Main()

    top_bar_params = TopBarParams(newListener=new_click, aboutListener=about_click, resetListener=reset_click,
                                  settingListener=setting_click)
    func_bar_params = FuncBarParams(sstfListener=sstf_click, fcfsListener=fcfs_click, scanListener=scan_click,
                                    cscanListener=cscan_click)
    mainWin = UI.CreateMainWindow(screen, top_bar_params=top_bar_params, func_bar_params=func_bar_params)

    mainWin.show()

    sys.exit(mainApp.exec())
"""
    ['E:\\pythonProject\\op-disk\\main.py',
    'E:\\pythonProject\\op-disk\\var.py',
    'E:\\pythonProject\\op-disk\\control/func_control.py',
    'E:\\pythonProject\\op-disk\\control/main_control.py',
    'E:\\pythonProject\\op-disk\\control/setting_control.py',
    'E:\\pythonProject\\op-disk\\core/Once.py',
    'E:\\pythonProject\\op-disk\\gui/ui_about.py',
    'E:\\pythonProject\\op-disk\\gui/ui_main.py',
    'E:\\pythonProject\\op-disk\\gui/ui_setting.py',
    'E:\\pythonProject\\op-disk\\utils/delLayout.py',
    'E:\\pythonProject\\op-disk\\utils/RunBack.py',
    'E:\\pythonProject\\op-disk\\utils/std.py',
    'E:\\pythonProject\\op-disk\\widget/DrawWidget.py',
    'E:\\pythonProject\\op-disk\\workThread/CSCANWorker.py',
    'E:\\pythonProject\\op-disk\\workThread/FCFSWorker.py',
    'E:\\pythonProject\\op-disk\\workThread/SCANWorker.py',
    'E:\\pythonProject\\op-disk\\workThread/SSTFWorker.py'
    ],
"""