import random

import var
from control.setting_control import new_process
from gui.ui_about import CreateAboutWindow
from gui.ui_setting import SettingParams, CreateSettingWindow

from var import core, screen, author


def new_click(ui_main):
    core.reset()
    for i in range(var.rand_num):
        core.addTime(random.randint(0, var.max_id))
    ui_main.refreshPages(core.getTimes())


def setting_click(ui_main):
    new_params = SettingParams(new_process)
    newWin = CreateSettingWindow(screen, new_params, ui_main)
    newWin.show()
    newWin.exec()



def about_click():
    print(author)
    aboutWin = CreateAboutWindow(screen)
    aboutWin.show()
    aboutWin.exec()


def reset_click(ui_main):
    core.reset()
    ui_main.refreshPages(core.getTimes())
    ui_main.refreshCurrent([])
    ui_main.refreshOutput("已重置")
