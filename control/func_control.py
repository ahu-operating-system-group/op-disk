import var
from var import core
from workThread.CSCANWorker import CSCANWorker
from workThread.FCFSWorker import FCFSWorker
from workThread.SCANWorker import SCANWorker
from workThread.SSTFWorker import SSTFWorker


def fcfs_click(ui_main):
    print("receive run_click")
    global worker
    worker = FCFSWorker(core.getTimes(), len(core.getTimes()), var.direct)
    worker.draw_label_signal.connect(ui_main.refreshCurrent)
    worker.output_signal.connect(ui_main.refreshOutput)

    worker.start()


def sstf_click(ui_main):
    print("receive run_click")
    global worker
    worker = SSTFWorker(core.getTimes(), len(core.getTimes()), var.direct)
    worker.draw_label_signal.connect(ui_main.refreshCurrent)
    worker.output_signal.connect(ui_main.refreshOutput)

    worker.start()


def scan_click(ui_main):
    print("receive run_click")
    global worker
    worker = SCANWorker(core.getTimes(), len(core.getTimes()), var.direct)
    worker.draw_label_signal.connect(ui_main.refreshCurrent)
    worker.output_signal.connect(ui_main.refreshOutput)

    worker.start()


def cscan_click(ui_main):
    print("receive run_click")
    global worker
    worker = CSCANWorker(core.getTimes(), len(core.getTimes()), var.direct)
    worker.draw_label_signal.connect(ui_main.refreshCurrent)
    worker.output_signal.connect(ui_main.refreshOutput)

    worker.start()
