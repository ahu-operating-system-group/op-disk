import var


def new_process(win, time_split_input, next_id_input, max_id_input, rand_num_input, direct_input,
                ui_main):
    win.close()
    var.time_split = int(time_split_input)
    var.next_id = int(next_id_input)
    var.max_id = int(max_id_input)
    var.rand_num = int(rand_num_input)
    if int(direct_input) > 0:
        var.direct = 1
    else:
        var.direct = 0
