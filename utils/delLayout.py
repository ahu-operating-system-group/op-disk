def delLayout(layout):
    item_list = list(range(layout.count()))
    item_list.reverse()  # 倒序删除，避免影响布局顺序

    for i in item_list:
        item = layout.itemAt(i)
        layout.removeItem(item)
        if item.widget():
            item.widget().deleteLater()
