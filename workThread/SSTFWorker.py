import time

from PyQt5.QtCore import QThread, pyqtSignal

import var
from utils.RunBack import Run_FCFS, Run_SSTF, Run_SCAN, Run_CSCAN, Run_Caculate


class SSTFWorker(QThread):
    draw_label_signal = pyqtSignal(list)

    output_signal = pyqtSignal(str)

    def __init__(self, mission, count, direction):
        super(SSTFWorker, self).__init__()
        self.page_requests = mission.copy()
        self.count = count
        self.direction = direction
        fcfs_time = Run_Caculate(Run_FCFS(mission.copy(), var.next_id, var.max_id, var.direct))
        sstf_time = Run_Caculate(Run_SSTF(mission.copy(), var.next_id, var.max_id, var.direct))
        scan_time = Run_Caculate(Run_SCAN(mission.copy(), var.next_id, var.max_id, var.direct))
        cscan_time = Run_Caculate(Run_CSCAN(mission.copy(), var.next_id, var.max_id, var.direct))
        self.all_put = f"FCFS的平均寻道时间{fcfs_time}\n"
        self.all_put += f"SSTF的平均寻道时间{sstf_time}\n"
        self.all_put += f"SCAN的平均寻道时间{scan_time}\n"
        self.all_put += f"CSCAN的平均寻道时间{cscan_time}\n"

    def generateOut(self, queue):
        out_str = "方法: SSTF\n"
        out_str += f"方向{self.direction}\n"
        out_str += f"访问的磁道序列为:\n{queue}\n"
        sum_gap = sum([(abs(queue[i] - queue[i - 1])) for i in range(1, len(queue))])
        out_str += f'移动的磁道数为：{sum_gap}\n'
        out_str += f'平均移动的磁道数为：{sum_gap / self.count}'
        return out_str

    TRACK_MAX_COUNT = var.max_id

    def findNearest(self, current, track_request, visited):
        minDis = self.TRACK_MAX_COUNT
        minIndex = -1
        for i in range(len(track_request)):
            if visited[i] == False:
                dis = abs(current - track_request[i])
                if dis < minDis:
                    minDis = dis
                    minIndex = i
        visited[minIndex] = True
        return minIndex, minDis

    def run(self):
        print("receive run")
        visited = [False] * self.count
        queue_SSTF = []
        current = var.next_id  # 起始的磁道
        for i in range(len(self.page_requests) + 1):
            index, dis = self.findNearest(current, self.page_requests, visited)
            queue_SSTF.append(current)
            current = self.page_requests[index]
        out_put = []
        for val in queue_SSTF:
            out_put.append(val)
            self.draw_label_signal.emit(out_put)
            time.sleep(var.time_split)
        out_put = self.generateOut(queue_SSTF)
        out_put += "\n" + self.all_put
        self.output_signal.emit(out_put)
        self.generateOut(queue_SSTF)
        print("finish")
