class Once:
    def __init__(self):
        self.times = []

    def reset(self):
        self.times = []

    def addTime(self, time):
        self.times.append(time)

    def getTimes(self):
        return self.times.copy()
        pass
