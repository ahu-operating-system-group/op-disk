# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['E:\\pythonProject\\op-disk\\main.py',
    'E:\\pythonProject\\op-disk\\var.py',
    'E:\\pythonProject\\op-disk\\control/func_control.py',
    'E:\\pythonProject\\op-disk\\control/main_control.py',
    'E:\\pythonProject\\op-disk\\control/setting_control.py',
    'E:\\pythonProject\\op-disk\\core/Once.py',
    'E:\\pythonProject\\op-disk\\gui/ui_about.py',
    'E:\\pythonProject\\op-disk\\gui/ui_main.py',
    'E:\\pythonProject\\op-disk\\gui/ui_setting.py',
    'E:\\pythonProject\\op-disk\\utils/delLayout.py',
    'E:\\pythonProject\\op-disk\\utils/RunBack.py',
    'E:\\pythonProject\\op-disk\\utils/std.py',
    'E:\\pythonProject\\op-disk\\widget/DrawWidget.py',
    'E:\\pythonProject\\op-disk\\workThread/CSCANWorker.py',
    'E:\\pythonProject\\op-disk\\workThread/FCFSWorker.py',
    'E:\\pythonProject\\op-disk\\workThread/SCANWorker.py',
    'E:\\pythonProject\\op-disk\\workThread/SSTFWorker.py'
    ],
    pathex=[],
    binaries=[],
    datas=[],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='main',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='main',
)
