from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtWidgets import QWidget


class DrawWidget(QWidget):
    def __init__(self, y_dip, x_scale):
        super().__init__()
        self.queue = []
        self.head = 0
        self.y_dip = y_dip
        self.x_scale = x_scale

    def set_queue(self, queue):
        self.queue = queue

    def set_head(self, head):
        self.head = head

    def paintEvent(self, event):
        painter = QPainter(self)
        pen = QPen(Qt.blue)
        painter.setPen(pen)
        painter.setRenderHint(QPainter.Antialiasing)

        for i in range(len(self.queue) - 1):
            disk = self.queue[i + 1]

            painter.drawLine(self.head * self.x_scale, i * self.y_dip, disk * self.x_scale, (i + 1) * self.y_dip)
            painter.drawText(disk * self.x_scale, (i + 1) * self.y_dip, str(disk))

            self.head = disk

    def reset(self):
        self.queue = []
        self.head = 0
        self.update()


